/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TrigT2Tau_page 
@author Olga Igonkina
@author M. Pilar Casado

@section TrigT2Tau_TrigT2TauOverview Overview
This package contains L2 tau trigger FEX algorithm which combines
tracking and calorimeter reconstruction information and fills TrigTau object.



*/
